package Repository;

import Model.Personas;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonasRepository extends JpaRepository <Personas, Long> {
}
