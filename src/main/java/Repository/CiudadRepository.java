package Repository;

import Model.Ciudad;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CiudadRepository extends JpaRepository<Ciudad, Long> {
    }