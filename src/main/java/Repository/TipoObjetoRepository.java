package Repository;

import Model.TipoObjeto;
import org.springframework.data.jpa.repository.JpaRepository;


public interface TipoObjetoRepository extends JpaRepository<TipoObjeto, Long> {
    }